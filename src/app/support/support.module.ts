import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupportRoutingModule } from './support-routing.module';
import { KnowledgebaseComponent } from './knowledgebase/knowledgebase.component';
import { SupportComponent } from './support.component';
import { MyServiceComponent } from './my-service/my-service.component';


@NgModule({
  declarations: [KnowledgebaseComponent, SupportComponent, MyServiceComponent],
  imports: [
    CommonModule,
    SupportRoutingModule
  ]
})
export class SupportModule { }
