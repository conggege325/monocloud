import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-service',
  templateUrl: './my-service.component.html',
  styleUrls: ['./my-service.component.scss']
})
export class MyServiceComponent implements OnInit {

  constructor( private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.route.snapshot.params['serviceId']);
  }

}
