import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupportComponent } from './support.component';
import { KnowledgebaseComponent } from './knowledgebase/knowledgebase.component';
import { MyServiceComponent } from './my-service/my-service.component';


const routes: Routes = [
  {
    path: '',
    component: SupportComponent,
    children: [
      { path: '', redirectTo: 'knowledgebase', pathMatch: 'full' },
      { path: 'service/:serviceId', component: MyServiceComponent },
      { path: 'knowledgebase', component: KnowledgebaseComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
